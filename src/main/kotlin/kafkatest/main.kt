package kafkatest

import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.runBlocking
import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.clients.producer.RecordMetadata
import org.apache.kafka.common.serialization.StringSerializer
import org.apache.kafka.common.serialization.UUIDSerializer
import java.util.*

fun <T> deferAsync(block: CompletableDeferred<T>.() -> Unit): Deferred<T> {
    val result = CompletableDeferred<T>()
    result.block()
    return result
}

fun <K, V> KafkaProducer<K, V>.sendAsync(topic: String, key: K, value: V): Deferred<RecordMetadata> = deferAsync {
    this@sendAsync.send(ProducerRecord(topic, key, value)) { metadata: RecordMetadata, exception: Exception? ->
        if (exception != null) completeExceptionally(exception)
        else complete(metadata)
    }
}

