package kafkatest.gui

import java.awt.Component
import java.awt.GridBagConstraints
import java.awt.Insets
import javax.swing.JPanel

val PADDING = 4
val DEFAULT_INSETS = Insets(PADDING, PADDING, PADDING, PADDING)
val NO_INSETS = Insets(0, 0, 0, 0)

fun <T: Component> JPanel.grid(
    component: T,
    x: Int = 0,
    y: Int = 0,
    colspan: Int = 1,
    rowspan: Int = 1,
    wx: Double = 0.0,
    wy: Double = 0.0,
    anchor: Int = GridBagConstraints.CENTER,
    fill: Int = GridBagConstraints.BOTH,
    insets: Insets = DEFAULT_INSETS
): T {
    this.add(component, GridBagConstraints(x, y, colspan, rowspan, wx, wy, anchor, fill, insets, 0, 0))
    return component
}

typealias ComponentAdder<T> = (component: T) -> T

infix fun <T: Component> T.at(init: GridBagConstraints.() -> Unit): T {
    TODO("Doesn't work. Would depend on something called 'compound extension', which is not implemented")
    val constraints = GridBagConstraints(
        0,
        0,
        1,
        1,
        0.0,
        0.0,
        GridBagConstraints.CENTER,
        GridBagConstraints.BOTH,
        DEFAULT_INSETS,
        0,
        0,
    )
    constraints.init()
    // ????.add(this, constraints)
    return this
}
