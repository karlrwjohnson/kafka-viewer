package kafkatest.gui

import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.common.TopicPartition
import org.apache.kafka.common.errors.WakeupException
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.kafka.common.serialization.UUIDDeserializer
import java.awt.*
import java.awt.event.WindowEvent
import java.awt.event.WindowListener
import java.time.Duration
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*
import javax.swing.*
import javax.swing.WindowConstants.DISPOSE_ON_CLOSE
import javax.swing.table.DefaultTableColumnModel
import javax.swing.table.TableColumn


val TOPIC = "my-topic"
val GROUP_ID = "viewer-" + UUID.randomUUID().toString()

val formatDate = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").withZone(ZoneId.systemDefault())::format

val SETTINGS = Properties().also {
    it["bootstrap.servers"] = "localhost:9092"
    it["acks"] = "all"
    it["group.id"] = GROUP_ID
}

typealias Record = ConsumerRecord<String, String>

data class Column<T>(val header: String, val getter: T.() -> Any, val width: Int = 75)

val COLUMNS = listOf(
    Column("Timestamp", { formatDate(Instant.ofEpochMilli(timestamp())) }, width = 50),
    Column("Partition", Record::partition, width = 30),
    Column("Key", Record::key),
    Column("Value", Record::value)
)

// How long consumer.poll() waits for a message.
// Far as I can tell, this can be as long as we want.
// We can abort this by calling consumer.wakeup() -> which throws a WakeupException
val KAFKA_POLL_SECONDS = 30L


enum class WakeupReason {
    TOPIC_CHANGED,
    EXITING
}

fun main() {
    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

    val consumer = KafkaConsumer(SETTINGS, StringDeserializer(), StringDeserializer())
    var wakeupReason: WakeupReason? = null
    val wakeup = { reason: WakeupReason ->
        wakeupReason = reason
        consumer.wakeup()
    }

    val topicListModel = DefaultComboBoxModel(consumer.listTopics().keys.toTypedArray())

    val columnModel = DefaultTableColumnModel().apply {
        COLUMNS.forEachIndexed { index, column ->
            addColumn(TableColumn(index, column.width).apply { headerValue = column.header })
        }
    }

    val tableModel = ReadOnlyTableModel(COLUMNS)

    var topic: String = consumer.listTopics().keys.iterator().next()

    JFrame("Kafka").apply {
        defaultCloseOperation = DISPOSE_ON_CLOSE
        contentPane = JPanel(GridBagLayout()).apply {
            grid(JLabel("Topic"), x = 0, y = 0)
            grid(JComboBox(topicListModel).apply { addActionListener {
                topic = this.selectedItem as String
                wakeup(WakeupReason.TOPIC_CHANGED)
                tableModel.truncate()
            } }, x = 1, y = 0, wx = 1.0)
            grid(JScrollPane(JTable(tableModel, columnModel).apply { fillsViewportHeight = true }), x = 0, y = 1, colspan = 2, wy = 1.0)
        }
        minimumSize = Dimension(800, 600)
        pack()
        setLocationRelativeTo(null)
        isVisible = true

        addWindowListener(BaseWindowListener(
            onClosing = { wakeup(WakeupReason.EXITING) }
        ))
    }

    Thread {
        val updateSubscription = { topic: String ->
            // Subscribe to every partition in the topic
            // as opposed to consumer.subscribe(listOf("my-topic"))
            // According to the docs, this overwrites any previous subscription -- which is PERFECT for what we need!
            val partitions = consumer.partitionsFor(topic).map { TopicPartition(it.topic(), it.partition()) }
            consumer.assign(partitions)
            consumer.seekToBeginning(partitions)
        }
        updateSubscription(topic)

        while (true) {
            val records = try {
                consumer.poll(Duration.ofSeconds(KAFKA_POLL_SECONDS))
            } catch (e: WakeupException) {
                // Determine why we were woken up
                when (wakeupReason) {
                    WakeupReason.EXITING -> {
                        println("Exiting kafka consumer")
                        return@Thread
                    }
                    WakeupReason.TOPIC_CHANGED -> {
                        updateSubscription(topic)
                    }
                    null -> {
                        println("Woken for no reason?")
                    }
                }
                wakeupReason = null
                continue
            }
            if (records.isEmpty) continue;

            val recordList = records.asIterable().toList()
            tableModel.addRows(recordList) // this might not happen on the right thread...
            consumer.commitAsync()
        }
    }.apply { start() }
}
