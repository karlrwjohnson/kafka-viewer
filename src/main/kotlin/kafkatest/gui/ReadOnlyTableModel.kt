package kafkatest.gui

import java.lang.UnsupportedOperationException
import javax.swing.event.TableModelEvent
import javax.swing.event.TableModelListener
import javax.swing.table.TableModel

class ReadOnlyTableModel<T>(
    val columns: List<Column<T>>
): TableModel {
    private val rows = mutableListOf<T>()

    fun addRows(newRows: List<T>) {
        val evt = TableModelEvent(this, rows.size, rows.size + newRows.size)
        rows.addAll(newRows)
        listeners.forEach { it.tableChanged(evt) }
    }

    fun truncate() {
        val evt = TableModelEvent(this, rows.size, rows.size)
        rows.clear()
        listeners.forEach { it.tableChanged(evt) }
    }


    override fun getRowCount(): Int = rows.size

    override fun getColumnName(col: Int): String = columns[col].header

    override fun isCellEditable(row: Int, col: Int): Boolean = false

    override fun getColumnClass(p0: Int): Class<*> = String::class.java

    override fun setValueAt(value: Any?, row: Int, col: Int) {
        throw UnsupportedOperationException("Table is read-only")
    }

    override fun getColumnCount(): Int = columns.size

    override fun getValueAt(row: Int, col: Int): Any = columns[col].getter(rows[row])

    val listeners =
        LinkedHashSet<TableModelListener>()
    override fun addTableModelListener(listener: TableModelListener) { listeners.add(listener) }
    override fun removeTableModelListener(listener: TableModelListener) { listeners.remove(listener) }

}