package kafkatest.gui

import java.awt.event.WindowEvent
import java.awt.event.WindowListener

class BaseWindowListener(
    val onDeiconified: ((evt: WindowEvent) -> Unit) = {},
    val onClosing: ((evt: WindowEvent) -> Unit) = {},
    val onClosed: ((evt: WindowEvent) -> Unit) = {},
    val onActivated: ((evt: WindowEvent) -> Unit) = {},
    val onDeactivated: ((evt: WindowEvent) -> Unit) = {},
    val onOpened: ((evt: WindowEvent) -> Unit) = {},
    val onIconified: ((evt: WindowEvent) -> Unit) = {},
): WindowListener {
    override fun windowDeiconified(evt: WindowEvent) = onDeiconified(evt)
    override fun windowClosing(evt: WindowEvent) = onClosing(evt)
    override fun windowClosed(evt: WindowEvent) = onClosed(evt)
    override fun windowActivated(evt: WindowEvent) = onActivated(evt)
    override fun windowDeactivated(evt: WindowEvent) = onDeactivated(evt)
    override fun windowOpened(evt: WindowEvent) = onOpened(evt)
    override fun windowIconified(evt: WindowEvent) = onIconified(evt)
}
