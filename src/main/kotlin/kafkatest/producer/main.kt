package kafkatest.producer

import kafkatest.sendAsync
import kotlinx.coroutines.runBlocking
import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.common.serialization.StringSerializer
import org.apache.kafka.common.serialization.UUIDSerializer
import java.util.*

fun main() = runBlocking {
    val producer = KafkaProducer(
        Properties().also {
            it["bootstrap.servers"] = "localhost:9092"
            it["acks"] = "all"
        }, UUIDSerializer(), StringSerializer()
    )

    producer.sendAsync("my-topic", UUID.randomUUID(), "bar").await();

    {}()
}
