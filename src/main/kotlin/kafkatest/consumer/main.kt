package kafkatest.consumer

import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.common.TopicPartition
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.kafka.common.serialization.UUIDDeserializer
import java.time.Duration
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*

val TOPIC = "my-topic"
val GROUP_ID = "viewer-" + UUID.randomUUID().toString()

val formatDate = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").withZone(ZoneId.systemDefault())::format

val SETTINGS = Properties().also {
    it["bootstrap.servers"] = "localhost:9092"
    it["acks"] = "all"
    it["group.id"] = GROUP_ID
}

fun main() {
    val consumer = KafkaConsumer(SETTINGS, UUIDDeserializer(), StringDeserializer())

    val topics = consumer.listTopics()
    topics.entries.forEach { (k, v) -> println("- $k") }

    // Subscribe to every partition in the topic
    // as opposed to consumer.subscribe(listOf("my-topic"))
    consumer.assign(consumer.partitionsFor(TOPIC).map { TopicPartition(it.topic(), it.partition()) })

    consumer.seekToBeginning(consumer.partitionsFor(TOPIC).map { TopicPartition(it.topic(), it.partition()) })

    while (true) {
        val records = consumer.poll(Duration.ofSeconds(5))
        if (records.isEmpty) break;
        for (record in records) {
            println("${formatDate(Instant.ofEpochMilli(record.timestamp()))}  ${record.topic()}[${record.partition()}]  ${record.key()}  ${record.value()}")
        }
        consumer.commitAsync()
    }
}
